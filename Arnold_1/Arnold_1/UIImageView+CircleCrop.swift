//
//  UIImageView+CircleCrop.swift
//  Arnold
//
//
//  Created by Simon Bozhilov on 04/02/15.
//  Copyright (c) 2015 Simon Bozhilov. All rights reserved.
//

import UIKit

extension UIImage{
    //overding UIImage function cropToCircleWithBorderColor
    func cropToCircleWithBorderColor(color: UIColor, lineWidth: CGFloat) -> UIImage{
        //returns image else aways will error!
        let imgRect = CGRect(origin: CGPointZero, size: self.size)
        UIGraphicsBeginImageContext(imgRect.size)
        var context = UIGraphicsGetCurrentContext()
        
        CGContextAddEllipseInRect(context, imgRect)
        CGContextClip(context)
        self.drawAtPoint(CGPointZero)
        
        CGContextAddEllipseInRect(context, imgRect)
        color.setStroke()
        CGContextSetLineWidth(context, lineWidth)
        CGContextStrokePath(context)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        return newImage
    }
}