//
//  ViewController.swift
//  Arnold
//
//  Created by Simon Bozhilov on 30/01/15.
//  Copyright (c) 2015 Simon Bozhilov. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    
    //Create and connect UIImageView and ULable
    @IBOutlet weak var arnold: UIImageView!
    @IBOutlet weak var message: UILabel!
    
    //inisialzying dictionary and audio player
    let imagesNames = ["Teriminator","Comando", "Arnold", "old", "Conan"]
    let soundName = ["20centuryFox", "choper", "Dolt", "Hasta la vista", "tumor", "talkToTheHand"]
    var soundPlayer = AVAudioPlayer()
    
    
    //function creating view
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let aSelector : Selector = "redisplay"
        let tapGuesture = UITapGestureRecognizer(target: self, action: aSelector)
        tapGuesture.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGuesture)
        //-----------------
        //arnold.image = UIImage(named: "A	rnold")
        //message.text = pickAnImage()
        //----------------
        redisplay()
        
    }
    
    //redisplaying function
    func redisplay()
    {
        let pickedImage = pickAnImage()
        let pickedSound = pickSound()
        
        let bundle = NSBundle.mainBundle().pathForResource(pickSound(), ofType: "aiff" )
        
        if let sound = NSURL.fileURLWithPath(bundle!){
            soundPlayer = AVAudioPlayer(contentsOfURL: sound, error: nil)
            soundPlayer.prepareToPlay()
            soundPlayer.play()
        }
        // arnold.image = UIImage( named: pickedImage)
        message.text = pickedImage
        arnold.image = UIImage(named: pickedImage)?.cropToCircleWithBorderColor(UIColor.blackColor(), lineWidth:20.0)
        
    }
    
    //randomly pick image from imagesNames dictionary
    func pickAnImage() -> String{
        return imagesNames[Int (arc4random_uniform(UInt32 (imagesNames.count)))]
    }
    
    //randomly pick sound from soundName dictionary
    func pickSound() -> String{
        return soundName[Int (arc4random_uniform(UInt32 (soundName.count)))]
    }
}

